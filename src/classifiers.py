from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
import config as cf


def knn_1():
    return KNeighborsClassifier(n_neighbors=1)


def knn_2():
    return KNeighborsClassifier(n_neighbors=2)


def knn_3():
    return KNeighborsClassifier(n_neighbors=3)


def knn_5():
    return KNeighborsClassifier(n_neighbors=5)


def knn_10():
    return KNeighborsClassifier(n_neighbors=10)


def decision_tree():
    return DecisionTreeClassifier(criterion="entropy", max_depth=3)


def random_forest_25():
    return RandomForestClassifier(n_estimators=25)


def random_forest_50():
    return RandomForestClassifier(n_estimators=50)


def random_forest_75():
    return RandomForestClassifier(n_estimators=75)


def random_forest_100():
    return RandomForestClassifier(n_estimators=100, max_depth =3)


classifiers = {

    #0: knn_1,

    #1: knn_2,

    #2: knn_3,

    #3: knn_5,

    0: knn_10,

    1: decision_tree,

    #6: random_forest_25,

    #7: random_forest_50,

    #8: random_forest_75,

    2: random_forest_100

}

markers = {

    0: '+',

    1: '+',

    2: '+',

    3: '+',

    4: '+',

    5: 'o',

    6: '*',

    7: '*',

    8: '*',

    9: '*'

}


def calc_operations(classifier, index):
    if classifiers[index].__name__ == 'knn_1':
        l = cf.features['features_num']
        k = 1
        o_knn = k * (l * l) * (l - 1) + 1
        return o_knn

    if classifiers[index].__name__ == 'knn_2':
        l = cf.features['features_num']
        k = 2
        o_knn = k * l * l * (l - 1) + 1
        return o_knn

    if classifiers[index].__name__ == 'knn_3':
        l = cf.features['features_num']
        k = 3
        o_knn = k * l * l * (l - 1) + 1
        return o_knn

    if classifiers[index].__name__ == 'knn_5':
        l = cf.features['features_num']
        k = 4
        o_knn = k * l * l * (l - 1) + 1
        return o_knn

    if classifiers[index].__name__ == 'knn_10':
        l = cf.features['features_num']
        k = 10
        o_knn = k * l * l * (l - 1) + 1
        return o_knn

    if classifiers[index].__name__ == 'decision_tree':
        o_tree = classifier.tree_.max_depth
        return o_tree

    if classifiers[index].__name__.startswith('random_forest'):
        o_rf = sum([estimator.get_depth() for estimator in classifier.estimators_])
        return o_rf

    else:
        print('No operation function is available for this classifier. Please check the calc_ops function.')
        return

