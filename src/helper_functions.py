from sklearn.model_selection import KFold

import data_prep
import load_datafiles

import numpy as np
import random

# define helper functions


def select_action(Q, s, eps):
    n = random.random()

    if n < eps:
        action_num = len(Q[s])
        a = random.randint(0, action_num - 1)

    else:
        a = np.argmax(Q[s])

    return a

