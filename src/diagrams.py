import matplotlib.pyplot as plt

import load_datafiles
from classifiers import classifiers, markers, calc_operations
from data_prep import Dataset
from hierarchical_classification import HierarchicalClassificationModel
from flat_classification import FlatClassificationModel
from q_agent import QAgent

dff = load_datafiles.load_data()


def get_score_diagram(splits_num):
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()
    fig4, ax4 = plt.subplots()

    beta = 1

    for clf in classifiers:
        accuracy = []
        precision = []
        recall = []
        f_measure = []
        ops = []

        dataset = Dataset(*dff)
        flat = FlatClassificationModel(dataset)
        flat.set_classifier(clf)

        hier = HierarchicalClassificationModel(dataset)
        hier.set_classifiers([clf, clf, clf, clf, clf])

        f = flat.get_scores_k_fold(splits=splits_num)
        h = hier.get_scores_k_fold(splits=splits_num)

        accuracy.append(1 - f[0][0])
        accuracy.append(1 - h[0][0])

        precision.append(f[0][1])
        precision.append(h[0][1])

        recall.append(f[0][2])
        recall.append(h[0][2])

        f_measure_flat = ((beta * beta + 1) * f[0][1] * f[0][2]) / ((beta * beta) * f[0][1] + f[0][2])
        f_measure_hier = ((beta * beta + 1) * h[0][1] * h[0][2]) / ((beta * beta) * h[0][1] + h[0][2])

        f_measure.append(f_measure_flat)
        f_measure.append(f_measure_hier)
        ops.append(f[1])
        ops.append(h[1])

        marker = markers[clf]

        ax1.scatter(ops, accuracy, alpha=1,
                    s=25, c=['g', 'r'], marker=marker, cmap='viridis')

        ax2.scatter(ops, precision, alpha=1,
                    s=25, c=['g', 'r'], marker=marker, cmap='viridis')

        ax3.scatter(ops, recall, alpha=1,
                    s=25, c=['g', 'r'], marker=marker, cmap='viridis')

        ax4.scatter(ops, f_measure, alpha=1,
                    s=25, c=['g', 'r'], marker=marker, cmap='viridis')

    mixed = HierarchicalClassificationModel(dataset)
    mixed.set_classifiers([5, 5, 6, 6, 6])
    m = mixed.get_scores_k_fold()
    f_measure = ((beta * beta + 1) * m[0][1] * m[0][2]) / ((beta * beta) * m[0][1] + m[0][2])

    ax1.scatter([m[1]], [1 - m[0][0]], alpha=1,
                s=25, c=['b'], marker='v', cmap='viridis')
    ax2.scatter([m[1]], [m[0][1]], alpha=1,
                s=25, c=['b'], marker='v', cmap='viridis')
    ax3.scatter([m[1]], [m[0][2]], alpha=1,
                s=25, c=['b'], marker='v', cmap='viridis')
    ax4.scatter([m[1]], [f_measure], alpha=1,
                s=25, c=['b'], marker='v', cmap='viridis')

    mixed_2 = HierarchicalClassificationModel(dataset)
    mixed_2.set_classifiers([5, 6, 6, 6, 6])
    m_2 = mixed_2.get_scores_k_fold()
    f_measure = ((beta * beta + 1) * m_2[0][1] * m_2[0][2]) / ((beta * beta) * m_2[0][1] + m_2[0][2])

    ax1.scatter([m_2[1]], [1 - m_2[0][0]], alpha=1,
                s=25, c=['b'], marker='^', cmap='viridis')
    ax2.scatter([m_2[1]], [m_2[0][1]], alpha=1,
                s=25, c=['b'], marker='^', cmap='viridis')
    ax3.scatter([m_2[1]], [m_2[0][2]], alpha=1,
                s=25, c=['b'], marker='^', cmap='viridis')
    ax4.scatter([m_2[1]], [f_measure], alpha=1,
                s=25, c=['b'], marker='^', cmap='viridis')

    ax1.set_ylabel('error')
    ax2.set_ylabel('HP')
    ax3.set_ylabel('HR')
    ax4.set_ylabel('HF')

    ax1.set_xscale('log')
    ax2.set_xscale('log')
    ax3.set_xscale('log')
    ax4.set_xscale('log')

    ax1.set_xlabel('operations')
    ax2.set_xlabel('operations')
    ax3.set_xlabel('operations')
    ax4.set_xlabel('operations')

    plt.show()


def get_gamma_hf_diagram(gamma_clfs, splits_num):
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    beta = 1

    f_measures = []
    ops = []

    dataset = Dataset(*dff)

    for clfs in gamma_clfs:
        clf = HierarchicalClassificationModel(dataset)
        clf.set_classifiers([clfs[0], clfs[1], clfs[2]])

        h = clf.get_scores_k_fold(splits=splits_num)
        f_measure = ((beta * beta + 1) * h[0][1] * h[0][2]) / ((beta * beta) * h[0][1] + h[0][2])

        f_measures.append(f_measure)
        ops.append(h[1])

        x = [x * 0.1 for x in range(len(gamma_clfs))]

        ax1.plot(x, f_measures, linestyle='solid', label='HF')
        ax2.plot(x, ops, color='orange', linestyle='solid', label='operations')

        ax1.set_xlabel('Gamma')
        ax1.set_ylabel('HF')
        ax1.legend(loc='upper left')

        ax2.set_xlabel('Gamma')
        ax2.set_ylabel('operations')
        ax2.legend(loc='upper left')

        plt.show()


##########################

def get_classifiers_for_gamma(subset_num):
    # to get which classifier is chosen for which level for different values for gamma
    # where gamma = 0 con
    q_agent = QAgent(*dff)
    q_agent.subset_data(subset_num)

    opt_policies = []
    for i in range(10):
        q_agent.gamma = i * 0.1
        q_agent.learn()
        opt_policies.append(q_agent.optimal_policy)

    return opt_policies


if __name__ == "__main__":
    get_classifiers_for_gamma(10)
