import data_prep
import load_datafiles

from classifiers import classifiers, markers, calc_operations

from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import KFold
from sklearn import metrics
from sklearn.model_selection import train_test_split, LeaveOneOut

import matplotlib.pyplot as plt

import numpy as np


class Classification:

    def __init__(self, t, *df):

        self.type = t
        self.dataset = df
        self.instances_num = len(df[0])
        self.sample_lengths = df[2]
        self.y_ext = df[3]

        if self.type == 'flat':
            self.x = df[0]
            self.y = data_prep.prep_labels(self.y_ext, state='severity')

            self.clf_flat = DecisionTreeClassifier(criterion="entropy")
            self.flat_index = 1

        elif self.type == 'hierarchical':
            self.binary_data = df[0], df[1]
            self.fault_data = data_prep.prep(*df, state='fault')
            self.sev_data_inner = data_prep.prep(*df, state='severity', fault='inner')
            self.sev_data_ball = data_prep.prep(*df, state='severity', fault='ball')
            self.sev_data_outer = data_prep.prep(*df, state='severity', fault='outer')

            self.clf_binary = DecisionTreeClassifier(criterion="entropy")
            self.clf_fault = DecisionTreeClassifier(criterion="entropy")
            self.clf_sev_inner = DecisionTreeClassifier(criterion="entropy")
            self.clf_sev_ball = DecisionTreeClassifier(criterion="entropy")
            self.clf_sev_outer = DecisionTreeClassifier(criterion="entropy")

            self.binary_index = 1
            self.fault_index = 1
            self.sev_index = 1

    def set_classifiers(self, *clfs):

        if self.type == 'flat':
            self.clf_flat = classifiers[clfs[0]]()
            self.flat_index = clfs[0]

            if len(clfs) > 1:
                print('A flat classifier can only have one classifier type!')

        elif self.type == 'hierarchical':

            if len(clfs) != 3:
                print('A hierarchical classifier needs 3 classifier types.')
                print('If all levels have the same type, please reenter the classifier index '
                      'separately for every level.')
                print('The output you will get is for a default decision tree classifier applied on every level.')

            else:
                self.clf_binary = classifiers[clfs[0]]()
                self.clf_fault = classifiers[clfs[1]]()

                self.clf_sev_inner = classifiers[clfs[2]]()
                self.clf_sev_ball = classifiers[clfs[2]]()
                self.clf_sev_outer = classifiers[clfs[2]]()

                self.binary_index = clfs[0]
                self.fault_index = clfs[1]
                self.sev_index = clfs[2]

    def fit_binary(self, train, test):
        x = self.binary_data[0]
        y = self.binary_data[1]

        x_train, y_train = x.loc[train], y.loc[train]

        self.clf_binary = self.clf_binary.fit(x_train, y_train)
        ops_binary = calc_operations(self.clf_binary, self.binary_index)

        return ops_binary

    def fit(self, test, level):

        if level == 'fault':
            x = self.fault_data[0]
            y = self.fault_data[1]

        elif level == 'inner':
            x = self.sev_data_inner[0]
            y = self.sev_data_inner[1]

        elif level == 'ball':
            x = self.sev_data_ball[0]
            y = self.sev_data_ball[1]

        elif level == 'outer':
            x = self.sev_data_outer[0]
            y = self.sev_data_outer[1]

        else:
            print('Level not defined. Please check fit function.')
            print('Available levels are: faultinner, ball, outer')
            print('In case of binary, please use fit_binary function')
            return

        for instance_index in test:
            if instance_index in x.index:
                x = x.drop(instance_index)
                y = y.drop(instance_index)

        if level == 'fault':
            self.clf_fault = self.clf_fault.fit(x, y)
            ops = calc_operations(self.clf_fault, self.fault_index)

        elif level == 'inner':
            self.clf_sev_inner = self.clf_sev_inner.fit(x, y)
            ops = calc_operations(self.clf_sev_inner, self.sev_index)

        elif level == 'ball':
            self.clf_sev_ball = self.clf_sev_ball.fit(x, y)
            ops = calc_operations(self.clf_sev_ball, self.sev_index)

        elif level == 'outer':
            self.clf_sev_outer = self.clf_sev_outer.fit(x, y)
            ops = calc_operations(self.clf_sev_outer, self.sev_index)

        else:
            return

        return ops

    def get_scores_flat(self, train, test):

        self.clf_flat = self.clf_flat.fit(self.x.loc[train], self.y.loc[train])
        ops_flat = calc_operations(self.clf_flat, self.flat_index)

        y_pred = self.clf_flat.predict(self.x.loc[test])

        accuracy = metrics.accuracy_score(self.y.loc[test], y_pred)

        class_index = {0: 0, 207: 1, 214: 2, 221: 3, 307: 4, 314: 5, 321: 6, 407: 7, 414: 8, 421: 9}
        actual_freq = 10 * [0]

        for test_index in test:
            actual_class = class_index[self.y_ext[test_index][-1]]
            actual_freq[actual_class] = actual_freq[actual_class] + 1

        pred_freq = 10 * [0]

        hp = 10 * [0]
        hr = 10 * [0]
        i = 0
        for index in test:
            predicted_class_index = class_index[y_pred[i]]
            pred_freq[predicted_class_index] = pred_freq[predicted_class_index] + 1

            if y_pred[i] == 0:
                y_pred_ext = [y_pred[i]]

            else:
                y_pred_ext = [1, int(y_pred[i] / 100), y_pred[i]]

            hp[predicted_class_index] = hp[predicted_class_index] \
                                        + len(set(y_pred_ext).intersection(self.y_ext[index])) / len(y_pred_ext)

            actual_index = class_index[self.y_ext[index][-1]]
            hr[actual_index] = hr[actual_index] + \
                               len(set(y_pred_ext).intersection(self.y_ext[index])) / len(self.y_ext[index])

            i = i + 1

        # precision = metrics.precision_score(self.y.loc[test], y_pred, average='weighted')
        # recall = metrics.recall_score(self.y.loc[test], y_pred, average='weighted')

        hpp = np.array(hp, dtype=np.float)
        pred_freq = np.array(pred_freq, dtype=np.float)

        hrr = np.array(hr, dtype=np.float)
        actual_freq = np.array(actual_freq, dtype=np.float)

        precision = sum(np.nan_to_num(hpp / pred_freq)) / 10
        recall = sum(np.nan_to_num(hrr / actual_freq)) / 10

        return (accuracy, precision, recall), ops_flat

    def get_scores(self, train, test):

        x = self.dataset[0]

        if self.type == 'flat':
            return self.get_scores_flat(train, test)

        # fit classifiers for every level and get the number
        # of operations required to predict by every classifier

        ops_binary = self.fit_binary(train, test)
        ops_fault = self.fit(test, 'fault')
        ops_inner = self.fit(test, 'inner')
        ops_ball = self.fit(test, 'ball')
        ops_outer = self.fit(test, 'outer')

        class_index = {0: 0, 207: 1, 214: 2, 221: 3, 307: 4, 314: 5, 321: 6, 407: 7, 414: 8, 421: 9}
        actual_freq = 10 * [0]

        pred_freq = 10 * [0]

        hp = 10 * [0]
        hr = 10 * [0]

        accuracy = 0
        ops = 0
        for index in test:

            actual_class = class_index[self.y_ext[index][-1]]
            actual_freq[actual_class] = actual_freq[actual_class] + 1

            instance = x.loc[index]
            y_pred_ext = []

            binary = self.clf_binary.predict([instance])
            y_pred_ext.append(binary[0])

            pred_freq[0] = pred_freq[0] + abs(binary[0] - 1)
            ops = ops + ops_binary

            if binary == 0:
                predicted_class = 0

            elif binary == 1:

                fault = self.clf_fault.predict([instance])
                y_pred_ext.append(fault[0])

                ops = ops + ops_fault

                if fault == 2:
                    sev = self.clf_sev_inner.predict([instance])
                    ops = ops + ops_inner

                elif fault == 3:
                    sev = self.clf_sev_ball.predict([instance])
                    ops = ops + ops_ball

                elif fault == 4:
                    sev = self.clf_sev_outer.predict([instance])
                    ops = ops + ops_outer

                else:
                    sev = None
                    print('Error fault not known!')

                y_pred_ext.append(sev[0])

                predicted_class = class_index[sev[0]]
                pred_freq[predicted_class] = pred_freq[predicted_class] + 1

            else:
                print('Could not identify if fault exists.')
                return

            accuracy = accuracy + (predicted_class == actual_class)

            hp[predicted_class] = hp[predicted_class] \
                                  + len(set(y_pred_ext).intersection(self.y_ext[index])) / len(y_pred_ext)

            hr[actual_class] = hr[actual_class] + \
                               len(set(y_pred_ext).intersection(self.y_ext[index])) / len(self.y_ext[index])

        accuracy = accuracy / sum(pred_freq)
        ops = ops / sum(pred_freq)

        hpp = np.array(hp, dtype=np.float)
        pred_freq = np.array(pred_freq, dtype=np.float)

        hrr = np.array(hr, dtype=np.float)
        actual_freq = np.array(actual_freq, dtype=np.float)

        hp_avg = sum(np.nan_to_num(hpp / pred_freq)) / 10
        hr_avg = sum(np.nan_to_num(hrr / actual_freq)) / 10

        return (accuracy, hp_avg, hr_avg), ops

    def get_scores_k_fold(self, splits=10):

        if self.type == 'flat':
            x = self.x

        else:
            x = self.binary_data[0]

        k_fold = KFold(n_splits=10, random_state=1, shuffle=True)

        accuracy = 0
        hp = 0
        hr = 0
        opers = 0

        for train, test in k_fold.split(x):
            (acc, hpp, hrr), op = self.get_scores(train, test)

            accuracy = accuracy + acc
            hp = hp + hpp
            hr = hr + hrr
            opers = opers + op

        return (accuracy / splits, hp / splits, hr / splits), opers / splits


if __name__ == "__main__":
    dff = load_datafiles.load_data()

beta = 1

'''for clf in classifiers:
    score = []
    ops = []

    flat = Classification('flat', *dff)
    flat.set_classifiers(clf)

    hier = Classification('hierarchical', *dff)
    hier.set_classifiers(clf, clf, clf)

    f = flat.get_scores_k_fold()
    h = hier.get_scores_k_fold()
    f_measure = ((beta * beta + 1) * h[0][1] * h[0][2]) / ((beta * beta) * h[0][1] + h[0][2])

    score.append(f[0][1])
    score.append(f_measure)
    ops.append(f[1])
    ops.append(h[1])

    marker = markers[clf]
    plt.scatter(ops, score, alpha=1,
                s=50, c=['g', 'r'], marker=marker, cmap='viridis')

mixed = Classification('hierarchical', *dff)
mixed.set_classifiers(5, 5, 6)
m = mixed.get_scores_k_fold()
f_measure = ((beta * beta + 1) * m[0][1] * m[0][2]) / ((beta * beta) * m[0][1] + m[0][2])

plt.scatter([m[1]], [f_measure], alpha=1,
            s=50, c=['b'], marker='v', cmap='viridis')

mixed_2 = Classification('hierarchical', *dff)
mixed_2.set_classifiers(5, 6, 6)
m_2 = mixed_2.get_scores_k_fold()
f_measure = ((beta * beta + 1) * m_2[0][1] * m_2[0][2]) / ((beta * beta) * m_2[0][1] + m_2[0][2])

plt.scatter([m_2[1]], [f_measure], alpha=1,
            s=50, c=['b'], marker='^', cmap='viridis')

plt.xlabel('operations')
plt.ylabel('HF')
plt.xscale('log')
plt.show()'''

k_fold = KFold(n_splits=1104, shuffle=True)
i = 0
for train, test in k_fold.split(dff[0]):
    print("train: " + str(train))
    print("test: " + str(test))
    i = i+1

print(i)
print(len(dff[0]))

