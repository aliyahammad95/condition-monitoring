from scipy.io import loadmat

import numpy as np
import pandas as pd

import config
import config as cf
import feature_extraction as fe


def load_data():
    # number of instances
    samples_num = cf.samples['samples_num']

    # the desired number of observations for every instance (time steps)
    observations_num = cf.samples['observations_num']
    # available datasets. please whenn adding more, add them in order:
    # normal, inner_7, inner_14, inner_21,
    # ball_7, ball_14, ball_21, outer_7, outer_14, outer_21

    datafiles = []
    for datafile in cf.datafiles:
        datafiles.append(datafile)

    datasets = []

    # define a dict to store the sample length of every data file
    # to be used later when assigning the labels based on the state (binary, fault, severity)

    sample_lengths = {}

    for datafile in datafiles:
        data = loadmat('../data/' + datafile + '.mat')

        # find the key that starts with X which indicates the actual data i.e permit
        # headers and so on, first one of these represents the time_series drive end accelerometer data

        data = [value for key, value in data.items() if key.startswith('X')]
        data = data[0]

        # Not all data files have enough observations to satisfy the desired number of samples
        # possible samples has to be calculated first

        possible_samples = int(len(data) / observations_num)

        if possible_samples > samples_num:
            possible_samples = samples_num

        sample_lengths[datafile] = possible_samples

        data = data[:possible_samples * observations_num]

        # Every value is in a list, that is why we need to flatten it first
        flat_data = [item for sublist in data for item in sublist]
        data = np.array(flat_data)

        # Split the whole data values into a number of np arrays that equals possible samples save the datafile name
        # to be used later in order to get the extended label y_extend see paper 'Hierarchical Fault Classification
        # for ResourceConstrained Systems' hierarichal metrics part to read more

        data = (np.split(data, possible_samples), datafile)

        datasets.append(data)

    # prepare the feature, by calculating statistical measures
    # length of features corresponds to number of instances
    # This dataset is not balanced
    # inner_14_0 has only 31 samples

    # give faults the corresponding number 2, 3, 4

    faults = config.faults

    features = []
    y_ext = []

    for dataset in datasets:
        for instance in dataset[0]:

            features.append(fe.extract_features(instance))

            # get the extended label based on the name of the file saved earlier
            # if it starts with 'normal' then set it to zero that means no fault
            # else give it the [1, the corresponding fault number, severity]
            # the extended label is always a list even if it has only one element
            # this is to be able to get the length to calculate recall in prediction

            ext = dataset[1].split('_')
            if ext[0].startswith('normal'):
                y_ext.append([0])
            else:
                y_ext.append([1, faults[ext[0]], 100 * faults[ext[0]] + int(ext[1])])

    # feautres can be added as desired

    feature_cols = fe.get_feature_names()
    x = pd.DataFrame(features, columns=feature_cols)

    # The first samples num of the dataset are no fault data, so they are set to zero
    # no fault = 0
    # fault = 1
    y = len(x) * [1]
    y[:sample_lengths['normal_0']] = sample_lengths['normal_0'] * [0]
    y = pd.DataFrame(y, columns=['label'])

    return x, y, sample_lengths, y_ext
