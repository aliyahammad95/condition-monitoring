import data_prep
import load_datafiles

from classifiers import classifiers, markers, calc_operations

from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import KFold
from sklearn import metrics

import numpy as np


class FlatClassificationModel:

    def __init__(self, dataset):

        self.dataset = dataset
        self.instances_num = dataset.get_instances_num()
        self.y_ext = dataset.get_y_ext()

        self.x = dataset.get_binary_data()[0]
        self.y = data_prep.prep_labels(self.y_ext, state='severity')

        self.clf_flat = DecisionTreeClassifier(criterion="entropy")
        self.flat_index = 1

    def set_classifier(self, clf):

        self.clf_flat = classifiers[clf]()
        self.flat_index = clf

    def get_scores(self, train, test):

        self.clf_flat = self.clf_flat.fit(self.x.loc[train], np.ravel(self.y.loc[train]))
        ops = calc_operations(self.clf_flat, self.flat_index)

        y_pred = self.clf_flat.predict(self.x.loc[test])

        accuracy = metrics.accuracy_score(self.y.loc[test], y_pred)

        class_index = {0: 0, 207: 1, 214: 2, 221: 3, 307: 4, 314: 5, 321: 6, 407: 7, 414: 8, 421: 9}
        actual_freq = 10 * [0]

        for test_index in test:
            actual_class = class_index[self.y_ext[test_index][-1]]
            actual_freq[actual_class] = actual_freq[actual_class] + 1

        pred_freq = 10 * [0]

        hp = 10 * [0]
        hr = 10 * [0]
        i = 0
        for index in test:
            predicted_class_index = class_index[y_pred[i]]
            pred_freq[predicted_class_index] = pred_freq[predicted_class_index] + 1

            if y_pred[i] == 0:
                y_pred_ext = [y_pred[i]]

            else:
                y_pred_ext = [1, int(y_pred[i] / 100), y_pred[i]]

            hp[predicted_class_index] = hp[predicted_class_index] \
                                        + len(set(y_pred_ext).intersection(self.y_ext[index])) / len(y_pred_ext)

            actual_index = class_index[self.y_ext[index][-1]]
            hr[actual_index] = hr[actual_index] + \
                               len(set(y_pred_ext).intersection(self.y_ext[index])) / len(self.y_ext[index])

            i = i + 1

        hpp = np.array(hp, dtype=np.float)
        pred_freq = np.array(pred_freq, dtype=np.float)

        hrr = np.array(hr, dtype=np.float)
        actual_freq = np.array(actual_freq, dtype=np.float)

        precision = sum(np.nan_to_num(hpp / (pred_freq + 0.001))) / 10
        recall = sum(np.nan_to_num(hrr / actual_freq)) / 10

        return (accuracy, precision, recall), ops

    def get_scores_k_fold(self, splits=10):

        x = self.x

        k_fold = KFold(n_splits=10, random_state=1, shuffle=True)

        accuracy = 0
        hp = 0
        hr = 0
        opers = 0

        for train, test in k_fold.split(x):
            (acc, hpp, hrr), op = self.get_scores(train, test)

            accuracy = accuracy + acc
            hp = hp + hpp
            hr = hr + hrr
            opers = opers + op

        return (accuracy / splits, hp / splits, hr / splits), opers / splits
