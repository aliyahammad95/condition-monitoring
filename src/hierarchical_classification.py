import config
from data_prep import Dataset
import load_datafiles
import timeit
from classifiers import classifiers, calc_operations
from sklearn.model_selection import KFold
import numpy as np


class HierarchicalClassificationModel:

    def __init__(self, data_set: Dataset, classifier_list=None) -> None:

        if classifier_list is None:
            classifier_list = 5 * [0]

        self.dataset = data_set
        self.instances_num = data_set.get_instances_num()
        self.y_ext = data_set.get_y_ext()

        self.binary_data = data_set.get_binary_data()
        self.fault_data = data_set.get_fault_data()
        self.sev_data_inner = data_set.get_inner_data()
        self.sev_data_ball = data_set.get_ball_data()
        self.sev_data_outer = data_set.get_outer_data()

        self.clf_binary = classifiers[classifier_list[0]]()
        self.clf_fault = classifiers[classifier_list[1]]()

        self.clf_sev_inner = classifiers[classifier_list[2]]()
        self.clf_sev_ball = classifiers[classifier_list[3]]()
        self.clf_sev_outer = classifiers[classifier_list[4]]()

        self.binary_index = classifier_list[0]
        self.fault_index = classifier_list[1]
        self.sev_index_inner = classifier_list[2]
        self.sev_index_ball = classifier_list[3]
        self.sev_index_outer = classifier_list[4]

    def set_classifiers(self, classifier_list):

        self.clf_binary = classifiers[classifier_list[0]]()
        self.clf_fault = classifiers[classifier_list[1]]()

        self.clf_sev_inner = classifiers[classifier_list[2]]()
        self.clf_sev_ball = classifiers[classifier_list[3]]()
        self.clf_sev_outer = classifiers[classifier_list[4]]()

        self.binary_index = classifier_list[0]
        self.fault_index = classifier_list[1]
        self.sev_index_inner = classifier_list[2]
        self.sev_index_ball = classifier_list[3]
        self.sev_index_outer = classifier_list[4]

    def set_classifier(self, level, fault, classifier_indx=None):

        if level == 'binary':
            if classifier_indx is not None:
                self.clf_binary = classifiers[classifier_indx]()
                self.binary_index = classifier_indx
            return self.clf_binary

        elif level == 'fault':
            if classifier_indx is not None:
                self.clf_fault = classifiers[classifier_indx]()
                self.fault_index = classifier_indx
            return self.clf_fault

        elif level == 'severity':
            if fault == 2:
                if classifier_indx is not None:
                    self.clf_sev_inner = classifiers[classifier_indx]()
                    self.sev_index_inner = classifier_indx
                return self.clf_sev_inner

            elif fault == 3:
                if classifier_indx is not None:
                    self.clf_sev_ball = classifiers[classifier_indx]()
                    self.sev_index_ball = classifier_indx
                return self.clf_sev_ball

            elif fault == 4:
                if classifier_indx is not None:
                    self.clf_sev_outer = classifiers[classifier_indx]()
                    self.sev_index_outer = classifier_indx

                return self.clf_sev_outer

    def subset_data(self, samples_num):

        self.dataset.subset_data(samples_num=samples_num)

        self.instances_num = self.dataset.get_instances_num()
        self.y_ext = self.dataset.get_y_ext()

        self.binary_data = self.dataset.get_binary_data()
        self.fault_data = self.dataset.get_fault_data()
        self.sev_data_inner = self.dataset.get_inner_data()
        self.sev_data_ball = self.dataset.get_ball_data()
        self.sev_data_outer = self.dataset.get_outer_data()

    def fit_binary(self, train):
        x = self.binary_data[0]
        y = self.binary_data[1]

        x_train, y_train = x.loc[train], y.loc[train]

        self.clf_binary = self.clf_binary.fit(x_train, np.ravel(y_train))
        ops_binary = calc_operations(self.clf_binary, self.binary_index)

        return ops_binary

    def fit_fault(self, test):

        x = self.fault_data[0]
        y = self.fault_data[1]

        for instance_index in test:
            if instance_index in x.index:
                x = x.drop(instance_index)
                y = y.drop(instance_index)

        self.clf_fault = self.clf_fault.fit(x, np.ravel(y))
        ops = calc_operations(self.clf_fault, self.fault_index)

        return ops

    def fit_severity(self, test, fault):
        if fault == 2:
            x = self.sev_data_inner[0]
            y = self.sev_data_inner[1]
            classifier = self.clf_sev_inner
            classifier_index = self.sev_index_inner

        elif fault == 3:
            x = self.sev_data_ball[0]
            y = self.sev_data_ball[1]
            classifier = self.clf_sev_ball
            classifier_index = self.sev_index_ball

        elif fault == 4:
            x = self.sev_data_outer[0]
            y = self.sev_data_outer[1]
            classifier = self.clf_sev_outer
            classifier_index = self.sev_index_outer

        else:
            print('Level not defined. Please check fit function.')
            print('Available levels are: inner, ball, outer')
            print('In case of binary, please use fit_binary function')
            return

        for instance_index in test:
            if instance_index in x.index:
                x = x.drop(instance_index)
                y = y.drop(instance_index)

        classifier.fit(x, np.ravel(y))
        ops = calc_operations(classifier, index=classifier_index)

        return ops

    def get_scores(self, train, test):

        x = self.binary_data[0]

        # fit classifiers for every level and get the number
        # of operations required to predict by every classifier

        ops_binary = self.fit_binary(train)
        ops_fault = self.fit_fault(test)
        ops_inner = self.fit_severity(test, 2)
        ops_ball = self.fit_severity(test, 3)
        ops_outer = self.fit_severity(test, 4)

        class_index = config.class_index

        # Make lists to
        actual_freq = 10 * [0]
        pred_freq = 10 * [0]
        hp = 10 * [0]
        hr = 10 * [0]

        accuracy = 0
        ops = 0
        for index in test:

            actual_class = class_index[self.y_ext[index][-1]]
            actual_freq[actual_class] = actual_freq[actual_class] + 1

            instance = x.loc[index]
            y_pred_ext = []

            binary = self.clf_binary.predict([instance])
            y_pred_ext.append(binary[0])

            ops = ops + ops_binary

            if binary == 0:
                predicted_class = 0
                pred_freq[0] = pred_freq[0] + 1

            elif binary == 1:

                fault = self.clf_fault.predict([instance])
                y_pred_ext.append(fault[0])

                ops = ops + ops_fault

                if fault == 2:
                    sev = self.clf_sev_inner.predict([instance])
                    ops = ops + ops_inner

                elif fault == 3:
                    sev = self.clf_sev_ball.predict([instance])
                    ops = ops + ops_ball

                elif fault == 4:
                    sev = self.clf_sev_outer.predict([instance])
                    ops = ops + ops_outer

                else:
                    sev = None
                    print('Error fault not known!')

                y_pred_ext.append(sev[0])

                predicted_class = class_index[sev[0]]
                pred_freq[predicted_class] = pred_freq[predicted_class] + 1

            else:
                print('Could not identify if fault exists.')
                return

            accuracy = accuracy + (predicted_class == actual_class)

            hp[predicted_class] = hp[predicted_class] \
                                  + len(set(y_pred_ext).intersection(self.y_ext[index])) / len(y_pred_ext)

            hr[actual_class] = hr[actual_class] + \
                               len(set(y_pred_ext).intersection(self.y_ext[index])) / len(self.y_ext[index])

        accuracy = accuracy / sum(pred_freq)
        ops = ops / sum(pred_freq)

        hpp = np.array(hp, dtype=float)
        pred_freq = np.array(pred_freq, dtype=float)

        hrr = np.array(hr, dtype=float)
        actual_freq = np.array(actual_freq, dtype=float)

        hp_avg = sum(np.nan_to_num(hpp / (pred_freq + 0.0001))) / 10
        hr_avg = sum(np.nan_to_num(hrr / actual_freq)) / 10

        return (accuracy, hp_avg, hr_avg), ops

    def get_scores_k_fold(self, splits=10):

        x = self.binary_data[0]

        k_fold = KFold(n_splits=splits, random_state=1, shuffle=True)

        accuracy = 0
        hp = 0
        hr = 0
        opers = 0

        for train, test in k_fold.split(x):
            (acc, hpp, hrr), op = self.get_scores(train, test)

            accuracy = accuracy + acc
            hp = hp + hpp
            hr = hr + hrr
            opers = opers + op

        return (accuracy / splits, hp / splits, hr / splits), opers / splits


if __name__ == "__main__":
    dff = load_datafiles.load_data()
    start = timeit.default_timer()
    clfs = [5, 5, 5, 5, 5]
    d = Dataset(*dff)
    cl = HierarchicalClassificationModel(d,  classifier_list=clfs)
    cl.subset_data(6)
    print(cl.get_scores_k_fold(splits=10))
    stop = timeit.default_timer()
    print('Time: ', stop - start)
