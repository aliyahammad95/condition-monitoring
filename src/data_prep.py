import pandas as pd
import load_datafiles
import numpy as np


# helper function

def prep_labels(labels, state):
    if state == 'binary':
        new_labels = [label[0] for label in labels]
        new_labels = pd.DataFrame(new_labels, columns=['label'])
        return new_labels

    if state == 'fault':
        new_labels = []
        for label in labels:
            if len(label) == 1:
                new_labels.append(0)

            else:
                new_labels.append(label[1])
        new_labels = pd.DataFrame(new_labels, columns=['label'])
        return new_labels

    if state == 'severity':
        new_labels = []
        for label in labels:
            if len(label) == 1:
                new_labels.append(0)

            else:
                new_labels.append(label[2])
        new_labels = pd.DataFrame(new_labels, columns=['label'])
        return new_labels


class Dataset:
    def __init__(self, *data_tuple: tuple) -> None:
        sample_lengths = data_tuple[2]
        self.y_ext = data_tuple[3]

        # get number of every different class, to be used later for splitting data
        self.normal = sum([value for key, value in sample_lengths.items() if key.startswith('normal')])

        self.inner_7 = sum([value for key, value in sample_lengths.items() if key.startswith('inner_7')])
        self.inner_14 = sum([value for key, value in sample_lengths.items() if key.startswith('inner_14')])
        self.inner_21 = sum([value for key, value in sample_lengths.items() if key.startswith('inner_21')])
        self.inner_total = self.inner_7 + self.inner_14 + self.inner_21

        self.ball_7 = sum([value for key, value in sample_lengths.items() if key.startswith('ball_7')])
        self.ball_14 = sum([value for key, value in sample_lengths.items() if key.startswith('ball_14')])
        self.ball_21 = sum([value for key, value in sample_lengths.items() if key.startswith('ball_21')])
        self.ball_total = self.ball_7 + self.ball_14 + self.ball_21

        self.outer_7 = sum([value for key, value in sample_lengths.items() if key.startswith('outer_7')])
        self.outer_14 = sum([value for key, value in sample_lengths.items() if key.startswith('outer_14')])

        self.x = data_tuple[0].copy()
        self.y = data_tuple[1].copy()
        self.instances_num = len(self.x)

        self.x_fault = None
        self.y_fault = None
        self.y_inner = None
        self.x_inner = None
        self.x_ball = None
        self.y_ball = None
        self.x_outer = None
        self.y_outer = None

        self.init_fault_labels()
        self.init_inner_labels()
        self.init_ball_labels()
        self.init_outer_labels()

    def subset_data(self, samples_num) -> None:
        fault_data = self.get_fault_data()
        sev_data_inner = self.get_inner_data()
        sev_data_ball = self.get_ball_data()
        sev_data_outer = self.get_outer_data()

        y_ext = [[0]] * samples_num

        # For testing purposes only
        samples_num_index = samples_num - 1

        sev_data_inner_7 = sev_data_inner[0].loc[120:120 + samples_num_index], \
                           sev_data_inner[1].loc[120:120 + samples_num_index]

        y_ext = y_ext + self.y_ext[120:120 + samples_num]

        sev_data_inner_14 = sev_data_inner[0].loc[238:238 + samples_num_index], \
                            sev_data_inner[1].loc[238: 238 + samples_num_index]

        y_ext = y_ext + self.y_ext[238: 238 + samples_num]

        sev_data_inner_21 = sev_data_inner[0].loc[270:270 + samples_num_index], \
                            sev_data_inner[1].loc[270:270 + samples_num_index]
        y_ext = y_ext + self.y_ext[270:270 + samples_num]

        sev_data_ball_7 = sev_data_ball[0].loc[400:400 + samples_num_index], \
                          sev_data_ball[1].loc[400:400 + samples_num_index]
        y_ext = y_ext + self.y_ext[400:400 + samples_num]

        sev_data_ball_14 = sev_data_ball[0].loc[500:500 + samples_num_index], \
                           sev_data_ball[1].loc[500:500 + samples_num_index]
        y_ext = y_ext + self.y_ext[500:500 + samples_num]

        sev_data_ball_21 = sev_data_ball[0].loc[630:630 + samples_num_index], \
                           sev_data_ball[1].loc[630:630 + samples_num_index]
        y_ext = y_ext + self.y_ext[630:630 + samples_num]

        sev_data_outer_7 = sev_data_outer[0].loc[760:760 + samples_num_index], \
                           sev_data_outer[1].loc[760:760 + samples_num_index]
        y_ext = y_ext + self.y_ext[760:760 + samples_num]

        sev_data_outer_14 = sev_data_outer[0].loc[870:870 + samples_num_index], \
                            sev_data_outer[1].loc[870:870 + samples_num_index]
        y_ext = y_ext + self.y_ext[870:870 + samples_num]

        sev_data_outer_21 = sev_data_outer[0].loc[1000:1000 + samples_num_index], \
                            sev_data_outer[1].loc[1000:1000 + samples_num_index]
        y_ext = y_ext + self.y_ext[1000:1000 + samples_num]

        self.y_ext = y_ext

        self.x_inner = pd.concat([sev_data_inner_7[0], sev_data_inner_14[0], sev_data_inner_21[0]], ignore_index=True)
        self.y_inner = pd.concat([sev_data_inner_7[1], sev_data_inner_14[1], sev_data_inner_21[1]], ignore_index=True)

        self.x_ball = pd.concat([sev_data_ball_7[0], sev_data_ball_14[0], sev_data_ball_21[0]], ignore_index=True)
        self.y_ball = pd.concat([sev_data_ball_7[1], sev_data_ball_14[1], sev_data_ball_21[1]], ignore_index=True)

        self.x_outer = pd.concat([sev_data_outer_7[0], sev_data_outer_14[0], sev_data_outer_21[0]], ignore_index=True)
        self.y_outer = pd.concat([sev_data_outer_7[1], sev_data_outer_14[1], sev_data_outer_21[1]], ignore_index=True)

        self.x_fault = pd.concat([self.x_inner, self.x_ball, self.x_outer],
                                 ignore_index=True)
        self.y_fault = pd.concat([np.floor(self.y_inner / 100.0), np.floor(self.y_ball / 100.0),
                                  np.floor(self.y_outer / 100.0)], ignore_index=True)
        self.y_fault = self.y_fault.astype(int)

        yf = self.y_fault.copy()
        yf['label'] = 1
        self.x = pd.concat([self.x.loc[0:samples_num_index], self.x_fault], ignore_index=True)
        self.y = pd.concat([self.y.loc[0:samples_num_index], yf], ignore_index=True)

        self.x_fault.index = self.x_fault.index + samples_num
        self.y_fault.index = self.y_fault.index + samples_num

        self.x_inner.index += samples_num
        self.y_inner.index += samples_num

        self.x_ball.index += samples_num * 4
        self.y_ball.index += samples_num * 4

        self.x_outer.index += samples_num * 7
        self.y_outer.index += samples_num * 7

        self.instances_num = len(self.x)

    def init_fault_labels(self) -> None:
        self.x_fault = self.x[self.normal:].copy()
        self.y_fault = self.y[self.normal:].copy()

        # Label every different fault with 2, 3, 4 respectively
        self.y_fault.loc[: self.inner_total + self.normal, 'label'] = 2
        self.y_fault.loc[self.inner_total + self.normal: self.ball_total + self.inner_total + self.normal, 'label'] = 3
        self.y_fault.loc[self.ball_total + self.inner_total + self.normal:, 'label'] = 4

    def init_inner_labels(self):
        self.x_inner = self.x_fault[:self.inner_total].copy()
        self.y_inner = self.y_fault[:self.inner_total].copy()

        # Set the labels based on severity
        self.y_inner[:self.inner_7] = 207
        self.y_inner[self.inner_7: self.inner_7 + self.inner_14] = 214
        self.y_inner[self.inner_7 + self.inner_14:] = 221

    def init_ball_labels(self):
        self.x_ball = self.x_fault[self.inner_total: self.inner_total + self.ball_total].copy()
        self.y_ball = self.y_fault[self.inner_total: self.inner_total + self.ball_total].copy()

        self.y_ball[:self.ball_7] = 307
        self.y_ball[self.ball_7: self.ball_7 + self.ball_14] = 314
        self.y_ball[self.ball_7 + self.ball_14:] = 321

    def init_outer_labels(self):
        self.x_outer = self.x_fault[self.ball_total + self.inner_total:].copy()
        self.y_outer = self.y_fault[self.ball_total + self.inner_total:].copy()

        self.y_outer[:self.outer_7] = 407
        self.y_outer[self.outer_7: self.outer_7 + self.outer_14] = 414
        self.y_outer[self.outer_7 + self.outer_14:] = 421

    def get_binary_data(self) -> tuple:
        return self.x, self.y

    def get_fault_data(self) -> tuple:
        return self.x_fault, self.y_fault

    def get_inner_data(self) -> tuple:
        return self.x_inner, self.y_inner

    def get_ball_data(self) -> tuple:
        return self.x_ball, self.y_ball

    def get_outer_data(self) -> tuple:
        return self.x_outer, self.y_outer

    def get_instances_num(self) -> int:
        return self.instances_num

    def get_y_ext(self) -> tuple:
        return self.y_ext


if __name__ == "__main__":
    dff = load_datafiles.load_data()
    d = Dataset(*dff)
    d.subset_data(6)
    print(d.get_fault_data())
