datafiles = [
    'normal_0',
    'inner_7_0',
    'inner_14_0',
    'inner_21_0',
    'ball_7_0',
    'ball_14_0',
    'ball_21_0',
    'outer_7_0',
    'outer_14_0',
    'outer_21_0'
]

samples = {
    'sampling_rate': 48000,

    # number of instances
    'samples_num': 1000,

    # the desired number of observations for every instance (time steps)
    'observations_num': 2048
}

features = {
    'mean': 1,
    'rms': 1,
    'skewness': 1,
    'kurtosis': 1,
    'foureir_max': 1,
    'foureir_mean': 0,
    'p_2': 0,
    'foreir_centroid': 0,
    'fourier peak': 0,

    'FC': 0,
    'RMSF': 0,
    'RVF': 0,
    'f_max': 0,
    'features_num': 5
}

q_agent = {
    "alpha": 0.05,
    "gamma": 1,
    "epsilon": 0.5,
    "reduction_factor": 0.9,
    "states": 15,
    "actions":3,
    "epochs": 1,
    "reps": 1,

    "alpha_reduction": 0.00005,
    "epsilon_reduction": 0.01
}

faults = {'inner': 2, 'ball': 3, 'outer': 4}

# Give every 'leaf node' class an index
class_index = {0: 0, 207: 1, 214: 2, 221: 3, 307: 4, 314: 5, 321: 6, 407: 7, 414: 8, 421: 9}
