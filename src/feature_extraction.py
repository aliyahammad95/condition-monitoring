import numpy as np

from scipy.stats import skew, kurtosis
from scipy.fft import fft, fftfreq
import config as cf


sampling_rate = cf.samples['sampling_rate']
observations_num = cf.samples['observations_num']

T = 1 / cf.samples['sampling_rate']


def mean(i):
    return np.mean(i)


def rms(i):
    mean_square = np.mean(i ** 2)
    return np.sqrt(mean_square)


def skewness(i):
    return skew(i)


def kurt(i):
    return kurtosis(i)


def foureir_max(i):
    f = fft(i)
    return np.max(2.0 / 2048 * np.abs(f[0:2048 // 2]))


def foureir_peak(i):
    f = fft(i)
    freqs = fftfreq(2048, T)[:2048 // 2]
    magnitudes = np.abs(f[0:2048 // 2])
    peak = np.argmax(magnitudes)
    return freqs[peak]


def foureir_centroid(i):
    f = fft(i)
    freqs = fftfreq(2048, T)[:2048 // 2]
    magnitudes = np.abs(f[0:2048 // 2])
    return np.sum(magnitudes * freqs) / np.sum(magnitudes)


def foureir_mean(i):
    f = fft(i)
    magnitudes = np.abs(f[0:2048 // 2])
    return np.mean(magnitudes)


def p_2(i):
    f = fft(i)
    magnitudes = np.abs(f[0:2048 // 2])
    mean = np.mean(magnitudes)
    return np.sum((magnitudes - mean) ** 2) / (len(magnitudes) - 1)


def FC(x):
    n_fft = observations_num
    amp = np.squeeze(np.abs(np.fft.rfft(np.squeeze(x), n_fft)))
    f = np.fft.rfftfreq(n_fft, T)
    return np.sum(f * amp) / np.sum(amp)


def RMSF(x):
    n_fft = observations_num
    amp = np.squeeze(np.abs(np.fft.rfft(np.squeeze(x), n_fft)))
    f = np.fft.rfftfreq(n_fft, T)
    return np.sqrt(np.sum(f ** 2 * amp) / np.sum(amp))


def RVF(x):
    n_fft = observations_num
    amp = np.squeeze(np.abs(np.fft.rfft(np.squeeze(x), n_fft)))
    f = np.fft.rfftfreq(n_fft, T)
    return np.sqrt(np.sum((f - FC(x)) ** 2 * amp) / np.sum(amp))


def f_max(x):
    n_fft = observations_num
    amp = np.squeeze(np.abs(np.fft.rfft(np.squeeze(x), n_fft)))
    f = np.fft.rfftfreq(n_fft, T)
    return f[np.argmax(amp)]


calc_features = {
    'mean': mean,
    'rms': rms,
    'skewness': skewness,
    'kurtosis': kurt,
    'foureir_max': foureir_max,
    'foureir_mean': foureir_mean,
    'p_2': p_2,
    'foreir_centroid': foureir_centroid,
    'fourier peak': foureir_peak,

    'FC': FC,
    'RMSF': RMSF,
    'RVF': RVF,
    'f_max': f_max
}


def extract_features(instance):
    features_values = []
    for feature in cf.features:
        if cf.features[feature] == 1:
            features_values.append(calc_features[feature](instance))

    return features_values


def get_feature_names():
    features_cols = []

    for feature in cf.features:
        if cf.features[feature] == 1:
            features_cols.append(feature)

    return features_cols
