from sklearn.model_selection import KFold
from data_prep import Dataset
import load_datafiles
import numpy as np
import config as cfg
from hierarchical_classification import HierarchicalClassificationModel
from numpy import exp

seed = 22345
rng = np.random.default_rng(seed)  # can be called without a seed


def softmax(vector):
    e = exp(vector)
    return e / e.sum()


def custom_argmax(np_list):
    x = np.where(np_list == np_list.max())[0]
    random_indx = rng.integers(0, len(x))
    return x[random_indx]


def get_classifiers_for_gamma(*dff):
    # to get which classifier is chosen for which level for different values for gamma
    # where gamma = 0 con

    optimal_policies = []
    for i in range(11):
        mc = MonteCarlo(*dff)
        mc.gamma = i * 0.1
        mc.e_soft()
        mc.e_soft()
        mc.set_optimal_policy()
        optimal_policies.append(mc.optimal_policy)
        print(i)

    return optimal_policies


class MonteCarlo:

    def __init__(self, *df):

        self.dataset = Dataset(*df)
        self.model = HierarchicalClassificationModel(self.dataset)
        self.x = self.model.binary_data[0]
        self.y = self.model.binary_data[1]
        self.y_ext = self.model.y_ext

        self.epsilon = 1
        self.gamma = cfg.q_agent["gamma"]

        self.reduction_factor = cfg.q_agent["reduction_factor"]
        self.states = cfg.q_agent["states"]
        self.actions = cfg.q_agent["actions"]
        self.epochs = cfg.q_agent["epochs"]

        self.alpha_reduction = cfg.q_agent["alpha_reduction"]
        self.epsilon_reduction = cfg.q_agent["epsilon_reduction"]

        self.q_matrix = np.zeros((self.states, self.actions))
        self.returns = {}

        random_vector = rng.random((self.states, self.actions))
        # softmax to get a convert the random values to probabilities
        self.policy = np.apply_along_axis(softmax, axis=1, arr=random_vector)
        self.optimal_policy = None

    def subset_data(self, samples_num):

        self.model.subset_data(samples_num=samples_num)
        self.x = self.model.binary_data[0]
        self.y = self.model.binary_data[1]
        self.y_ext = self.model.y_ext

    def get_action(self, state):
        '''n = rng.random()

        if n < self.epsilon:
            action = rng.integers(0, self.actions)

        else:
            action = custom_argmax(self.policy[state])'''

        action = rng.choice(self.actions, p=self.policy[state])
        return action

    def run_eposide(self, train, test):

        sar_list = []
        instance_index = test[0]
        x_1 = self.x.loc[instance_index]
        y_1 = self.y.loc[instance_index]

        state = 0
        action = self.get_action(state)

        self.model.set_classifier('binary', fault=None, classifier_indx=action)
        operations = self.model.fit_binary(train)
        y_pred = self.model.clf_binary.predict([x_1])
        reward = 100 * self.gamma * (y_pred[0] == y_1[0]) + 100* (1 - self.gamma) * ((np.log(1 / operations)+9.2)/9.2)
        #print(reward)

        sar_list.append((state, action, reward))

        # second state
        # if fault detected, perform fault classification
        if y_1[0] == 1:

            state = state + 2

            action = self.get_action(state)

            self.model.set_classifier('fault', fault=None, classifier_indx=action)

            operations = self.model.fit_fault(test)

            actual_fault = self.y_ext[instance_index]

            # Check that it is actually a fault and not missclassified
            if len(actual_fault) > 1:
                actual_fault = actual_fault[1]

            # because it returns a numpy array, we extract the value of int
            fault_prediction = self.model.clf_fault.predict([x_1])
            reward = self.gamma * 100 * (fault_prediction == actual_fault) + 100*(1 - self.gamma) * ((np.log(1 / operations)+9.2)/9.2)
            #print(reward)

            sar_list.append((state, action, reward[0]))

            # third state
            state = state + fault_prediction[0] - 1

            action = self.get_action(state)

            clf_severity = self.model.set_classifier('severity', fault=fault_prediction[0], classifier_indx=action)
            operations = self.model.fit_severity(test, fault=fault_prediction[0])
            actual_sev = self.y_ext[instance_index]

            # Check that it is a actually a fault, otherwise list index out of rage error
            if len(actual_sev) > 1:
                actual_sev = actual_sev[2]

            sev_prediction = clf_severity.predict([x_1])
            reward = self.gamma * 100 * (sev_prediction == actual_sev) +100* (1 - self.gamma) * ((np.log(1 / operations)+9.2)/9.2)
            #print(reward)

            sar_list.append((state, action, reward[0]))

        return sar_list

    def e_soft(self):

        k_fold = KFold(n_splits=self.model.instances_num, shuffle=True)
        k = 0
        for train, test in k_fold.split(self.x):
            k = k + 1
            #print(k)
            self.epsilon = max(self.epsilon - 0.003, 0.1)
            sar_list = self.run_eposide(train, test)
            G = 0
            sar_list_reversed = sar_list[::-1]
            for index, step in enumerate(sar_list_reversed):
                state = step[0]
                action = step[1]
                reward = step[2]
                G = G * self.reduction_factor + reward
                if (state, action) not in ((x[0], x[1]) for x in sar_list_reversed[index + 1:]):
                    if (state, action) in self.returns:
                        self.returns[state, action].append(G)
                    else:
                        self.returns[state, action] = [G]

                    self.q_matrix[state, action] = sum(self.returns[state, action]) / len(self.returns[state, action])
                    #print("state = " + str(state))
                    #print("action = " + str(action))
                    #print("Q =" + str(self.q_matrix[state]))
                    optimal_action = custom_argmax(self.q_matrix[state])
                    for act in range(self.actions):
                        if act == optimal_action:
                            self.policy[state, act] = 1 - self.epsilon + (self.epsilon / self.actions)
                        else:
                            self.policy[state, act] = self.epsilon / self.actions

    def set_optimal_policy(self):
        self.optimal_policy = []
        for state in range(6):
            if state == 1:
                continue
            self.optimal_policy.append(custom_argmax(self.policy[state]))

    def get_optimal_policy(self):
        return self.optimal_policy

