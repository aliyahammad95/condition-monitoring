from sklearn.model_selection import KFold

from data_prep import Dataset
import load_datafiles

import numpy as np
import pandas as pd

from classifiers import classifiers
from helper_functions import select_action
import config as cfg
from hierarchical_classification import HierarchicalClassificationModel


# define hyper-parameters of q learner

class QAgent:

    def __init__(self, *df):

        self.dataset = Dataset(*df)
        self.model = HierarchicalClassificationModel(self.dataset)
        self.x = self.model.binary_data[0]
        self.y = self.model.binary_data[1]
        self.y_ext = self.model.y_ext

        self.alpha = cfg.q_agent["alpha"]
        self.gamma = cfg.q_agent["gamma"]
        self.epsilon = cfg.q_agent["epsilon"]

        self.reduction_factor = cfg.q_agent["reduction_factor"]
        self.states = cfg.q_agent["states"]
        self.actions = cfg.q_agent["actions"]
        self.epochs = cfg.q_agent["epochs"]

        self.alpha_reduction = cfg.q_agent["alpha_reduction"]
        self.epsilon_reduction = cfg.q_agent["epsilon_reduction"]

        self.q_matrix = None
        self.optimal_policy = None
        self.accumulative_rewards = None

    def subset_data(self, samples_num):

        self.model.subset_data(samples_num=samples_num)
        self.x = self.model.binary_data[0]
        self.y = self.model.binary_data[1]
        self.y_ext = self.model.y_ext

    def learn(self):

        value_per_episode = [0] * self.epochs
        q_matrix = np.zeros((self.states, self.actions))

        for epoch in range(self.epochs):
            eps = 1

            k_fold = KFold(n_splits=self.model.instances_num, shuffle=True)
            for train, test in k_fold.split(self.x):

                instance_index = test[0]
                x_1 = self.x.loc[instance_index]
                y_1 = self.y.loc[instance_index]

                state = 0
                action = select_action(q_matrix, state, self.epsilon)

                self.model.clf_binary = classifiers[action]()
                operations = self.model.fit_binary(train)
                y_pred = self.model.clf_binary.predict([x_1])

                reward = 100 * self.gamma * (y_pred[0] == y_1[0]) + (1 - self.gamma) * np.log(1 / (operations + 0.1))

                if y_pred == [0]:
                    value_per_episode[epoch] += reward * self.reduction_factor ** eps
                    q_matrix[state][action] = q_matrix[state][action] + \
                                              self.alpha * (reward - q_matrix[state][action])

                    eps = eps + 1

                # second state
                # if fault detected, perform fault classification
                if y_pred == [1]:
                    value_per_episode[epoch] += reward * self.reduction_factor ** eps
                    q_matrix[state][action] = q_matrix[state][action] + \
                                              self.alpha * (reward + self.reduction_factor * np.max(q_matrix[state + 2])
                                                            - q_matrix[state][action])

                    state = state + 2

                    action = select_action(q_matrix, state, self.epsilon)
                    self.model.clf_fault = classifiers[action]()

                    operations = self.model.fit_fault(test)

                    actual_fault = self.y_ext[instance_index]

                    # Check that it is actually a fault and not missclassified
                    if len(actual_fault) > 1:
                        actual_fault = actual_fault[1]

                    # because it returns a numpy array, we extract the value of int
                    fault_prediction = self.model.clf_fault.predict([x_1])
                    reward = self.gamma * 100 * (fault_prediction == actual_fault) + (1 - self.gamma) * np.log(
                        1 / operations)
                    value_per_episode[epoch] += reward * self.reduction_factor ** eps
                    # print(reward)

                    q_matrix[state][action] = q_matrix[state][action] \
                                              + self.alpha * (reward + self.reduction_factor * np.max(
                        q_matrix[state + fault_prediction - 1]) - q_matrix[state][action])

                    # third state
                    state = state + fault_prediction - 1

                    action = select_action(q_matrix, state, self.epsilon)
                    clf_severity = classifiers[action]()
                    clf_severity = self.model.set_classifier('severity', fault_prediction[0], classifier=clf_severity)
                    operations = self.model.fit_severity(test, fault=fault_prediction[0])
                    actual_sev = self.y_ext[instance_index]

                    # Check that it is a actually a fault, otherwise list index out of rage error
                    if len(actual_sev) > 1:
                        actual_sev = actual_sev[2]

                    sev_prediction = clf_severity.predict([x_1])
                    reward = self.gamma * 100 * (sev_prediction == actual_sev) + (1 - self.gamma) * np.log(
                        1 / operations)
                    value_per_episode[epoch] += reward * self.reduction_factor ** eps

                    q_matrix[state][action] = q_matrix[state][action] + self.alpha * (reward - q_matrix[state][action])

                    eps = eps + 1

            self.alpha = self.alpha - self.alpha_reduction
            self.epsilon = self.epsilon - self.epsilon_reduction

        policy_opt = []

        for state in range(6):
            if state == 1:
                continue
            policy_opt.append(np.argmax(q_matrix[state]))

        self.q_matrix = q_matrix
        self.optimal_policy = policy_opt

        self.accumulative_rewards = value_per_episode

        return value_per_episode


if __name__ == "__main__":
    dff = load_datafiles.load_data()
    q = QAgent(*dff)
    q.subset_data(6)
    q.learn()
    print(q.optimal_policy)
