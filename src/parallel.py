import load_datafiles
import Qagent as q
import multiprocessing as mp
import numpy as np
from numpy import savetxt
import config as cfg

output = mp.Queue()


def learn(num, out):
    dff = load_datafiles.load_data()
    q_agent = q.Qagent(*dff)
    # q_agent.subset_data(1)
    val_per_episode = q_agent.learn()
    out.put(val_per_episode)


def learn_lambda(lam, out):
    dff = load_datafiles.load_data()
    q_agent = q.Qagent(*dff)

    q_agent.gamma = lam * 0.1
    q_agent.learn()
    out.put(q_agent.optimal_policy)


episodes = cfg.q_agent["epochs"]
reps = cfg.q_agent["reps"]
rewards = np.zeros((episodes, reps))

processes = [mp.Process(target=learn_lambda, args=(0.1 * x, output)) for x in range(reps)]

for p in processes:
    p.start()

for p in processes:
    p.join()

for rep in range(len(processes)):
    rewards[:, rep] = output.get()

print(rewards.mean(axis=1))
