import load_datafiles
from monte_carlo import MonteCarlo
import multiprocessing as mp

output = mp.Queue()
dff = load_datafiles.load_data()


mcc = []
for i in range(100):
    mcc.append(MonteCarlo(*dff))

def learn(x, out):
    mc = mcc[x]
    print(mc)
    mc.gamma = 1

    for i in range(50):
        print("i = "  + str(i))
        mc.e_soft()

    mc.set_optimal_policy()
    out.put(mc.get_optimal_policy())


reps = 10

processes = [mp.Process(target=learn, args=(x, output)) for x in range(reps)]

for p in processes:
    p.start()
    print(p)

for p in processes:
    p.join()
    print(p)

opti = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]

for rep in range(len(processes)):
    policy = output.get()
    print(policy)
    for i, x in enumerate(policy):
        opti[x][i] += 1


print(opti)
