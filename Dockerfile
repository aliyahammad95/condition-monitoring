FROM continuumio/miniconda3

#keep user privileges minimal 
RUN adduser --disabled-login  safeuser 

#add jupyter lab
RUN conda install jupyterlab

#add specific R kernel
RUN conda install -c conda-forge r=3.6 r-irkernel

#alternatively with more control
#RUN conda install --quiet --yes \
#    'r-base=4.0.3'  \
#    'r-caret=6.0*' \
#    'r-crayon=1.4*' \
#    'r-irkernel=1.1*' \
#    ...


#add python packages
WORKDIR /src
COPY . /src
RUN pip install -r requirements.txt

WORKDIR /usr/src/safeuser


RUN chown -R safeuser:safeuser ./
USER safeuser


EXPOSE 1234
