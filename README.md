###### This project is an implementation of the paper:  ["Hierarchical Fault Classification for Resource Constrained System"](https://www.sciencedirect.com/science/article/abs/pii/S0888327019304819)

###### Dataset: [Case Western University Bearing Fault Data](https://csegroups.case.edu/bearingdatacenter/pages/welcome-case-western-reserve-university-bearing-data-center-website)



###### Get the container running ######

1) Download and install Docker Desktop (Windows/Mac -> https://docs.docker.com/docker-for-windows/install/) or Compose (Linux -> https://docs.docker.com/compose/install/)

2) Clone the repository

3) Go the file where the repository is cloned

2) Write 'docker-compose up' to create container from image

5) Copy-paste IP (e.g. http://127.0.0.1:1234/lab) into your browser

6) Password for JupyterLab: 'learning'

7) Stop container with 'docker-compose stop'

8) In case you need to remove the container 'docker-compose down'


###### Please check out the following tutorials to get started: 

1) [Know The Data](https://gitlab.com/aliyahammad95/condition-monitoring/-/blob/master/src/Know%20the%20data.ipynb)

2) [My First Hierarchical Classifier](https://gitlab.com/aliyahammad95/condition-monitoring/-/blob/master/src/My%20First%20Hierarchical%20Classifier.ipynb)

3) [Q Learning](https://gitlab.com/aliyahammad95/condition-monitoring/-/blob/master/src/Q%20learning.ipynb)


